override['java']['jdk_version'] = '7'
default['tomcat']['enable_welcome_file_list'] = true

case node["deploy_env"]
when nil, ''
  env=''
when 'test'
  env='test'
when 'release','prod'
  env='prod'
when 'dev','ci'
  env='dev'
when 'int'
  env='int'
when 'NEB-2030'
  env='dev'
else
  env='test'
end

default["tomcat"]["max_http_header_size"] = 65536
default['tomcat']['catalina_options']="-Dorg.apache.cocoon.mode=#{env} \
-Dcom.sun.management.jmxremote.authenticate=false \
-Dcom.sun.management.jmxremote.port=17090 \
-Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote=true"

java_opts = "-Xmx12288m -Djava.awt.headless=true -server \
-XX:PermSize=2048m -XX:MaxPermSize=2048m -XX:-PrintGCDetails -XX:+UseParallelOldGC \
-Denv=#{env} \
-Dhttp.proxyHost=nibr-proxy.global.nibr.novartis.net -Dhttp.proxyPort=2011"

#if deploy_env == 'dev'
#    java_opts << " -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=39574"
#end

default[:tomcat][:java_options] = java_opts
override.nibr_ssl.enabled = true
override['tomcat']['use_ssl_enabled_protocols'] = true
node.override["tomcat"]["max_http_header_size"] = 65536
